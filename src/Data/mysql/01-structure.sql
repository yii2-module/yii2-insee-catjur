/**
 * Database structure required by InseeCatjurModule.
 *
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-insee-catjur
 * @license MIT
 */

SET foreign_key_checks = 0;

DROP TABLE IF EXISTS `insee_catjur_n1`;
DROP TABLE IF EXISTS `insee_catjur_n2`;
DROP TABLE IF EXISTS `insee_catjur_n3`;

SET foreign_key_checks = 1;

CREATE TABLE `insee_catjur_n1`
(
	`insee_catjur_n1_id` INT(11) NOT NULL PRIMARY KEY COMMENT 'The id of the juridic category lv1',
	`libelle` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The long libelle of this record'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the juridic category lv1 data';

CREATE TABLE `insee_catjur_n2`
(
	`insee_catjur_n2_id` INT(11) NOT NULL PRIMARY KEY COMMENT 'The id of the juridic category lv2',
	`insee_catjur_n1_id` INT(11) NOT NULL COMMENT 'The id of the related juridic category lv1',
	`libelle` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The long libelle of this record'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the juridic category lv2 data';

CREATE TABLE `insee_catjur_n3`
(
	`insee_catjur_n3_id` INT(11) NOT NULL PRIMARY KEY COMMENT 'The id of the juridic category lv3',
	`insee_catjur_n2_id` INT(11) NOT NULL COMMENT 'The id of the related juridic category lv2',
	`createdYear` INT(11) NOT NULL DEFAULT 1990 COMMENT 'The date when this category was created',
	`removedYear` INT(11) DEFAULT NULL COMMENT 'The date when this category was removed',
	`libelle` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The long libelle of this record'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the juridic category lv3 data';
