/**
 * Database relations required by InseeCatjurModule.
 *
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-insee-catjur
 * @license MIT
 */

ALTER TABLE `insee_catjur_n2` ADD CONSTRAINT `fk_insee_catjur_n2_insee_catjur_n1` FOREIGN KEY (`insee_catjur_n1_id`) REFERENCES `insee_catjur_n1`(`insee_catjur_n1_id`) ON DELETE RESTRICT ON UPDATE CASCADE; 

ALTER TABLE `insee_catjur_n3` ADD CONSTRAINT `fk_insee_catjur_n3_insee_catjur_n2` FOREIGN KEY (`insee_catjur_n2_id`) REFERENCES `insee_catjur_n2`(`insee_catjur_n2_id`) ON DELETE RESTRICT ON UPDATE CASCADE; 
