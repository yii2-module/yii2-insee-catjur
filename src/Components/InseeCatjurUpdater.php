<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-catjur library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeCatjur\Components;

use InvalidArgumentException;
use Iterator;
use PhpExtended\ApiFrInseeCatjur\ApiFrInseeCatjurCategoryLv1Interface;
use PhpExtended\ApiFrInseeCatjur\ApiFrInseeCatjurCategoryLv2Interface;
use PhpExtended\ApiFrInseeCatjur\ApiFrInseeCatjurCategoryLv3Interface;
use RuntimeException;
use Yii2Module\Helper\Components\ObjectUpdater;
use Yii2Module\Yii2InseeCatjur\Models\InseeCatjurN1;
use Yii2Module\Yii2InseeCatjur\Models\InseeCatjurN2;
use Yii2Module\Yii2InseeCatjur\Models\InseeCatjurN3;

/**
 * InseeCatjurUpdater class file.
 * 
 * THis class updates all the InseeCatjur records.
 * 
 * @author Anastaszor
 */
class InseeCatjurUpdater extends ObjectUpdater
{
	
	/**
	 * Updates all the juridic category lvl1 records.
	 * 
	 * @param Iterator<integer, ApiFrInseeCatjurCategoryLv1Interface> $level1s
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateCatjurLv1(Iterator $level1s) : int
	{
		return $this->saveIteratorEachClass(
			$level1s,
			InseeCatjurN1::class,
			function(ApiFrInseeCatjurCategoryLv1Interface $level1) : array
			{
				return [
					'insee_catjur_n1_id' => (int) $level1->getIdLv1(),
				];
			},
			function(ApiFrInseeCatjurCategoryLv1Interface $level1) : array
			{
				return [
					'libelle' => $level1->getLibelle(),
				];
			},
		);
	}
	
	/**
	 * Updates all the juridic category lvl2 records.
	 * 
	 * @param Iterator<integer, ApiFrInseeCatjurCategoryLv2Interface> $level2s
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateCatjurLv2(Iterator $level2s) : int
	{
		return $this->saveIteratorEachClass(
			$level2s,
			InseeCatjurN2::class,
			function(ApiFrInseeCatjurCategoryLv2Interface $level2) : array
			{
				return [
					'insee_catjur_n2_id' => (int) $level2->getIdLv2(),
				];
			},
			function(ApiFrInseeCatjurCategoryLv2Interface $level2) : array
			{
				return [
					'insee_catjur_n1_id' => $level2->getIdLv1(),
					'libelle' => $level2->getLibelle(),
				];
			},
		);
	}
	
	/**
	 * Updates all the juridic category lvl3 records.
	 * 
	 * @param Iterator<integer, ApiFrInseeCatjurCategoryLv3Interface> $level3s
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateCatjurLv3(Iterator $level3s) : int
	{
		return $this->saveIteratorEachClass(
			$level3s,
			InseeCatjurN3::class,
			function(ApiFrInseeCatjurCategoryLv3Interface $level3) : array
			{
				return [
					'insee_catjur_n3_id' => (int) $level3->getIdLv3(),
				];
			},
			function(ApiFrInseeCatjurCategoryLv3Interface $level3) : array
			{
				return [
					'insee_catjur_n2_id' => $level3->getIdLv2(),
					'libelle' => $level3->getLibelle(),
					'createdYear' => $level3->getCreatedYear(),
					'removedYear' => $level3->getRemovedYear(),
				];
			},
		);
	}
	
}
