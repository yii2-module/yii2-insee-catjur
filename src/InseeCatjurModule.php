<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-catjur library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeCatjur;

use yii\base\Module;
use yii\BaseYii;
use Yii2Extended\Metadata\Bundle;
use Yii2Extended\Metadata\Record;
use Yii2Module\Helper\BootstrappedModule;
use Yii2Module\Yii2InseeCatjur\Models\InseeCatjurN1;
use Yii2Module\Yii2InseeCatjur\Models\InseeCatjurN2;
use Yii2Module\Yii2InseeCatjur\Models\InseeCatjurN3;

/**
 * InseeCatjurModule class file.
 * 
 * This module is to represent the juridic category informations about french
 * juridical repartition of labour, to be able to build other resources that
 * count on those juridic structures.
 * 
 * @author Anastaszor
 */
class InseeCatjurModule extends BootstrappedModule
{
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getBootstrapIconName()
	 */
	public function getBootstrapIconName() : string
	{
		return 'book';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getLabel()
	 */
	public function getLabel() : string
	{
		return BaseYii::t('InseeCatjurModule.Module', 'Catjur');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getEnabledBundles()
	 */
	public function getEnabledBundles() : array
	{
		return [
			'catjur' => new Bundle(BaseYii::t('InseeCatjurModule.Module', 'Catjur'), [
				'n1' => (new Record(InseeCatjurN1::class, 'n1', BaseYii::t('InseeCatjurModule.Module', 'Catjur N1')))->enableFullAccess(),
				'n2' => (new Record(InseeCatjurN2::class, 'n2', BaseYii::t('InseeCatjurModule.Module', 'Catjur N2')))->enableFullAccess(),
				'n3' => (new Record(InseeCatjurN3::class, 'n3', BaseYii::t('InseeCatjurModule.Module', 'Catjur N3')))->enableFullAccess(),
			]),
		];
	}
	
}
