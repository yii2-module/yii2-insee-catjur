<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-catjur library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeCatjur\Commands;

use PhpExtended\ApiFrInseeCatjur\ApiFrInseeCatjurEndpoint;
use yii\console\ExitCode;
use Yii2Module\Helper\Commands\ExtendedController;
use Yii2Module\Yii2InseeCatjur\Components\InseeCatjurUpdater;

/**
 * UpdateController class file.
 * 
 * This command updates all parts of the insee juridic category database.
 * 
 * @author Anastaszor
 */
class UpdateController extends ExtendedController
{
	
	/**
	 * Updates all the classes of juridic category records.
	 * 
	 * @return integer the error code, 0 if no error
	 */
	public function actionAll() : int
	{
		return $this->runCallable(function() : int
		{
			$endpoint = new ApiFrInseeCatjurEndpoint();
			$updater = new InseeCatjurUpdater();
			$updater->setLogger($this->getLogger());
			$updater->updateCatjurLv1($endpoint->getJuridicCategoryLv1Iterator());
			$updater->updateCatjurLv2($endpoint->getJuridicCategoryLv2Iterator());
			$updater->updateCatjurLv3($endpoint->getJuridicCategoryLv3Iterator());

			return ExitCode::OK;
		});
	}
	
}
